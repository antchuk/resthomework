﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WebClient
{
    class Program
    {        
        static void Main(string[] args)
        {
            Console.WriteLine("Hi!\nHomework REST API\nInput client ID:");
            if (long.TryParse(Console.ReadLine(),out long _nextID))
            {
                //запрашиваем пользователя
                GetByID(_nextID);
                //генерируем нового пользователя
                Console.WriteLine("Input new client ID\n(Lastname and Firstname are generated automatically)");
                if (long.TryParse(Console.ReadLine(), out long _newID) && _newID>0)
                {
                    CustomerCreateRequest _NewCustomer = RandomCustomer();
                    long _newCustomer = TryCreateNewCustomer(_NewCustomer, _newID);
                    //отображаем нового ползователя
                    switch (_newCustomer)
                    {
                        case -404:
                            Console.WriteLine("404 Not found");
                            break;
                        case -409:
                            Console.WriteLine("409 Already exists");
                            break;
                        default:
                            GetByID(_newCustomer);
                            break;
                    }

                }
            }
            else
            {
                Console.WriteLine("ID parsing problems");
            }
        }
        
        private static HttpClient client = new HttpClient();
        /// <summary>
        /// запрос есть ли пользователь с указанным ID
        /// </summary>
        /// <param name="ID"></param>
        private static async void GetByID(long ID)
        {
            HttpResponseMessage response = client.GetAsync($"https://localhost:5001/customers/{ID}").Result;
            if (response.IsSuccessStatusCode) 
            {
                Customer _customer = response.Content.ReadFromJsonAsync<Customer>().Result;
                Console.WriteLine($"ID = {_customer.Id}\nFirstname = {_customer.Firstname}\nLastname = {_customer.Lastname}");
            }
            if (response.StatusCode==System.Net.HttpStatusCode.NotFound) 
            {
                Console.WriteLine("404 Not found");
            }
        }
        private static long TryCreateNewCustomer(CustomerCreateRequest NewCustomer, long _newID)
        {
            Customer _customer = new()
            { 
                Firstname = NewCustomer.Firstname, 
                Lastname = NewCustomer.Lastname,
                Id = _newID
            };

            HttpResponseMessage response = client.PostAsJsonAsync($"https://localhost:5001/customers/", _customer).Result;
            switch (response.StatusCode)
            {
                case System.Net.HttpStatusCode.NotFound:                    
                    return -404;
                case System.Net.HttpStatusCode.Conflict:                    
                    return -409;                    
                case System.Net.HttpStatusCode.OK:
                    long _customerID = response.Content.ReadFromJsonAsync<long>().Result;
                    return _customerID;
                default:
                    return -1;
            }        
        }
        private static CustomerCreateRequest RandomCustomer()
        {
            CustomerCreateRequest NewCustomer = new(10);
            return NewCustomer;
        }
    }
}