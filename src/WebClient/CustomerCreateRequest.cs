using System;
using System.Text;

namespace WebClient
{
    public class CustomerCreateRequest
    {
        public CustomerCreateRequest(int Length)
        {
            Firstname = GenerateRandomString(Length);
            Lastname = GenerateRandomString(Length);
        }
        private string GenerateRandomString(int Length)
        {
            Random rnd = new();
            string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder sb = new(Length - 1);
            int Position = 0;
            for (int i = 0; i < Length; i++)
            {
                Position = rnd.Next(0, Alphabet.Length - 1);
                sb.Append(Alphabet[Position]);
            }
            return sb.ToString();
        }
        /*public CustomerCreateRequest(
            string firstName,
            string lastName)
        {
            Firstname = firstName;
            Lastname = lastName;
        }*/

        public string Firstname { get; init; }

        public string Lastname { get; init; }
    }
}