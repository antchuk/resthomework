﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace WebApi.Infrastructure.EntityFramework
{
    public static class EntityFrameworkInstaller
    {
        public static IServiceCollection ConfigureContext(this IServiceCollection services,
            string connectionString)
        {
            services.AddDbContext<DatabaseContext>(optionsBuilder
                => optionsBuilder
                    //.UseLazyLoadingProxies() // lazy loading
                    .UseNpgsql(connectionString));
            //.UseSqlServer(connectionString));

            services.AddHealthChecks();
            //services.AddHealthChecks().AddDbContextCheck<DatabaseContext>();

            return services;
        }
    }
}
