﻿using AutoMapper;
using WebApi.Models;
using WebApi.Services.Services.Contracts;

namespace WebApi.Mapping
{
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile()
        {
            CreateMap<CustomerDto, Customer>();
            CreateMap<Customer, CustomerDto>();

            //CreateMap<CourseFilterModel, CourseFilterDto>();
        }
    }
}
