﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using WebApi.Infrastructure.EntityFramework;
using WebApi.Models;
using WebApi.Services.Services.Repositories.Abstractions;

namespace WebApi.Services.Services.Repositories.Implementations
{
    public class CustomerRepository : ICustomerRepository
    {
        public readonly DbContext Context;
        private readonly DbSet<Customer> _customerSet;
        public CustomerRepository(DatabaseContext context) //: base(context)
        {
            Context = context;
            
            _customerSet = Context.Set<Customer>();
        }
        public async Task<Customer> GetAsync(long id) 
        {            
            return await _customerSet.FindAsync(id);
        }
        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
        public async Task<Customer> CreateAsync(Customer customer)
        {
            //var _newcustomer = await _customer.AddAsync(customer);

            return (await _customerSet.AddAsync(customer)).Entity;
        }
    }
}
