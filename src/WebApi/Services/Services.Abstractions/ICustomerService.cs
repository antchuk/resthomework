﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Services.Services.Contracts;
using WebApi.Models;

namespace WebApi.Services.Services.Abstractions
{
    public interface ICustomerService
    {
        Task<CustomerDto> GetCustomerById(long id);
        Task<long> Create(CustomerDto customerDto);
    }
}
