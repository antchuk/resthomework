﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models;
using WebApi.Services.Services.Abstractions;
using WebApi.Services.Services.Repositories.Abstractions;
using WebApi.Services.Services.Contracts;
using AutoMapper;

namespace WebApi.Services
{
    
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IMapper _mapper;
        public CustomerService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }
        public async Task<CustomerDto> GetCustomerById(long id)
        {
            var customer = await _customerRepository.GetAsync(id);
            if (customer == null)
            {
                return null;
            }
            CustomerDto _cust = new()
            { 
                ID= customer.Id,
                FirstName=customer.Firstname,
                LastName=customer.Lastname
            };
            return _cust;
            //return _mapper.Map<Customer, CustomerDto>(customer);            
        }
        public async Task<long> Create(CustomerDto customerDto)
        {
            //проврека сущетвует ли такой уже
            var customer = await _customerRepository.GetAsync(customerDto.ID);
            if (customer != null) 
            {
                return -1;
            }
            Customer entity = new Customer()
            { 
                Firstname=customerDto.FirstName,
                Lastname=customerDto.LastName,
                Id=customerDto.ID
            };
            var res = await _customerRepository.CreateAsync(entity);
            await _customerRepository.SaveChangesAsync();
            return res.Id;
        }
    }
}
