﻿using AutoMapper;
using WebApi.Services.Services.Contracts;
using WebApi.Models;

namespace WebApi.Services.Services.Implementations.Mapping
{
    public class CustomerMappingsProfile : Profile
    {
        public CustomerMappingsProfile() 
        {
            CreateMap<Customer, CustomerDto>();
            CreateMap<CustomerDto, Customer>();/*
                .ForMember(d => d.ID, map => map.Ignore())
                .ForMember(d => d.FirstName, map => map.Ignore())
                .ForMember(d => d.LastName, map => map.Ignore());*/
        }
    }
}
