﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Services.Services.Contracts;
using WebApi.Models;

namespace WebApi.Services.Services.Repositories.Abstractions
{
    public interface ICustomerRepository
    {
        Task<Customer> GetAsync(long id);
        Task<Customer> CreateAsync(Customer item);
        Task SaveChangesAsync();
    }
}
