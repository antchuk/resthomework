using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using WebApi.Models;
//using WebApi.Services.Services.
using WebApi.Services;
using WebApi.Services.Services.Contracts;
using WebApi.Services.Services.Abstractions;

namespace WebApi.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomerController : ControllerBase
    {
        private ICustomerService _service;
        private IMapper _mapper;
        public CustomerController(ICustomerService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        [HttpGet("{id:long}")]   
        public async Task<IActionResult> GetCustomerAsync([FromRoute] long id)
        {
            var _customer = await _service.GetCustomerById(id);
            if (_customer == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<Customer>(_customer));
        }

        [HttpPost]   
        public async Task<IActionResult> CreateCustomerAsync([FromBody] CustomerDto customerDto)
        {
            var _customer = await _service.Create(_mapper.Map<CustomerDto>(customerDto));
            if (_customer == -1) 
            {
                return Conflict();
            }
            return Ok(_customer);
        }
    }
}